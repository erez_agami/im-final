#include "VideoStreaming.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdexcept>
#include <cmath>
#include "image_convertor.h"


VideoStreamer::VideoStreamer(int port){
    this->server = new Socket();
    this->port = port;
}

void send(Socket &socket, cv::Mat m){
    VideoFrame frame;
    frame._id = 1;
    frame.length = m.cols * m.rows * 3;
    frame.buffer = m.ptr();
    unsigned int sofar = 0;
    unsigned char *pointer = frame.buffer;
    
    while (sofar < frame.length){
        int toSend = fmin((int)frame.length - sofar,256);
        socket.send(pointer,toSend);
        sofar+= toSend;
        pointer+= toSend;
    }
    
}

VideoStreamer::~VideoStreamer(){
    this->stop();
}
    void VideoStreamer::start()
    {
        pleaseStop =false;
        Socket new_sock;
        VideoFrame frame;
        
        if (!this->server->create()){
            throw std::runtime_error("cannot create socket");
        }
        
        
        if (!this->server->bind(this->port)){
            throw std::runtime_error("cannot bind socket");
        }
        
        if (!this->server->listen()){
            throw std::runtime_error("cannot bind socket");
        }
        
        while(!pleaseStop){
            this->server->accept(new_sock);
        
            try
            {
                int sofar = 0;
                char header [16];
                while ( !pleaseStop )
                {  // read the string and write it back
                    char data[256];
                    int length;
                    length = new_sock.recv(data, 256);
               
                    for (int i=0; i< length;i++){
                        //ugly as hell
                        if (sofar< 16){
                            header[sofar++]=data[i];
                        }else{
                            if (16 == sofar){
                                frame._id = ntohl(*((int *)header));
                                frame.length = ntohl(*(((unsigned int *)header) + 1));
                                frame.lightCode = ntohl(*(((unsigned int *)header) + 2));
                                frame.modelCode = ntohl(*(((unsigned int *)header) + 3));
                                frame.buffer = (unsigned char*)malloc(frame.length);
                            }
                        
                            frame.buffer[sofar++ - 16]= data[i];
                        
                            if (sofar == frame.length+16){
                                printf("frame size data: %d", length);
                                cv::Mat m = im.convert((unsigned char *)    frame.buffer, frame.length);
                                m = this->_cb->onFrame(m, frame.modelCode, frame.lightCode);
                                sofar = 0;
                                send(new_sock,m);
                                free(frame.buffer);
                            }
                        
                        }
                    }
                }
            }
            catch ( std::runtime_error& ) {}
        }
    }



    void VideoStreamer::registerCallback(IStreamListener *cb){
        this->_cb = cb;
    }

    void VideoStreamer::stop(){
        pleaseStop = true;
        this->server->~Socket();
    }
    
