#ifndef __example2__image_convertor__
#define __example2__image_convertor__

#include <iostream>
#include <jpeglib.h>
#include <stdexcept>
#include <opencv2/opencv.hpp>

class ImageConvertor{
public:
    cv::Mat convert(unsigned char *buff, unsigned long length);
    
};

#endif /* defined(__example2__image_convertor__) */
