
#ifndef im_final_IFrameHandler_h
#define im_final_IFrameHandler_h
#include "Frame.hpp"
/*Interface for Pipline element that handle incoming frame from outside source
    e.g: camera, network...
 */
class IFrameHandler{
public:
    /*Handle incoming frame*/
    virtual void inFrame(PipFrame frame) = 0;
    /*Connect another IFrameHandler Element to be invoked after this finish handling the frame*/
    virtual void connect(IFrameHandler * nextHandler){
        this->next = nextHandler;
    }
    
protected:
    IFrameHandler *next = NULL;
};

#endif
