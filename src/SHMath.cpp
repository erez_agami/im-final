#include <stdio.h>
#include <math.h>

double factorial(int i){
    double x=1;
    for (int j=1; j<=i;j++){
        x*=j;
    }
    return x;
    
}
//The Function Calculate the Legendre Polynomial
//using 3 rules:
// 1. we can find P_m_m  using: (-1)^m(2m-1)!!(1-x^2)m^2
// 2. we can calculate the next P_m_m+1 using: x(2m+1)P_m_m
// 3. we can calcualte Pm_l using: (x(2l-1)P_m_l-1 - (l+m-1)P_m_l-2)/(l-m)
double Plegendre(int l, int m, double x){
    //calculating
    double pmm = 1.0;
    if(m>0){
        double somx2 = sqrt((1.0-x)*(1.0+x));
        double fact = 1.0;
        for (int i=1; i<=m; i++){
            pmm*= (-fact) * somx2;
            fact += 2.0;
        }
    }
    if (l==m) {//pmm we are done
        return pmm;
    }
    //calculating next by
    double pmmp1 = x * (2.0*m+1.0)*pmm;
    if (l == m+1){
        return pmmp1; //done
    }
    //calculting using 2 previous value
    double pill = 0.0;
    for (int ll=m+2; ll<= l; ++ll){
        pill = ( (2.0*ll-1.0)*x*pmmp1 - (ll+m-1.0)*pmm) / (ll-m);
        pmm = pmmp1;
        pmmp1 = pill;
    }
    return pill;
}
//renormalisation constant for SH function
double Knormalize(int l, int m){
    double temp = ((2.0*l+1.0)*factorial(l-m)) / (4.0*M_PI*factorial(l+m));
    return sqrt(temp);
}

//Calcualte the SH Function at point(spherical coord)
double SHFunc(int l, int m, double theta, double phi){
    //Spherical Harmonic basis function
    //l band
    //m range [-l..l]
    //theta [0..pi]
    //pho [0..2*pi]
    if(m==0){
        return Knormalize(l,0) * Plegendre(l,m,cos(theta));
    }else if (m>0){
        return M_SQRT2 * Knormalize(l,m) * cos(m*phi) * Plegendre(l,m,cos(theta));
    }else{
        return M_SQRT2 * Knormalize(l, -m) * sin(-m*phi) * Plegendre(l,-m,cos(theta));
    }
}