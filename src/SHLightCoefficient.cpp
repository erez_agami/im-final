#include <stdio.h>
#include <math.h>
#include "SphericalHarmonics.hpp"
#include "SHRotate.h"


SHLightCoefficient::SHLightCoefficient(const SHSamples *samples, const ILightSource * lightSource )
:numFunctions(samples->getNumFunctions())
, numBands(samples->getNumBands())
{


    
    const unsigned int numSamples = samples->getNumSamples();
    const SHSample * samplesArray = samples->samplesPtr();
    
    lightCoefficient = new glm::vec3[numFunctions];
    rotatedLightCoefficient = new glm::vec3[numFunctions];
    
    for (int i=0; i<numFunctions; ++i){
        lightCoefficient[i] = glm::vec3(0.0,0.0,0.0);
        for(int j=0; j < numSamples; ++j){
            glm::vec3 light_at_point = lightSource->light(samplesArray[j].theta, samplesArray[j].phi);
            lightCoefficient[i]+= light_at_point * samplesArray[j].shFuncVal[i];
        }
        lightCoefficient[i]*=4*M_PI/numSamples;
        rotatedLightCoefficient[i] = lightCoefficient[i];
    }
}

SHLightCoefficient::~SHLightCoefficient(){
    if (NULL != lightCoefficient ){
        delete lightCoefficient;
    }
    if (NULL != rotatedLightCoefficient){
        delete rotatedLightCoefficient;
    }
}
void SHLightCoefficient::rotate(double theta, double phi){
    for (int i=0; i<numFunctions; ++i){
        this->rotatedLightCoefficient[i] = this->lightCoefficient[i];
    }
    double tempx[numFunctions];
    double tempy[numFunctions];
    double tempz[numFunctions];
    double orgx[numFunctions];
    double orgy[numFunctions];
    double orgz[numFunctions];
    
    for (int i=0;i<numFunctions;i++){
        orgx[i] = tempx[i] = this->lightCoefficient[i].x;
        orgy[i] = tempy[i] = this->lightCoefficient[i].y;
        orgz[i] = tempz[i] = this->lightCoefficient[i].z;
    }
    
    RotateSHCoefficients(numBands, orgx,tempx,theta,phi);
    RotateSHCoefficients(numBands, orgy,tempy,theta,phi);
    RotateSHCoefficients(numBands, orgz,tempz,theta,phi);
    
    for (int i=0;i<numFunctions;i++){
        this->rotatedLightCoefficient[i].x = tempx[i];
        this->rotatedLightCoefficient[i].y = tempy[i];
        this->rotatedLightCoefficient[i].z = tempz[i];
    }
}

const glm::vec3 * SHLightCoefficient::getRotatedLightCoefficient() const{
    return this->rotatedLightCoefficient;
}

const unsigned int SHLightCoefficient::getNumFunctions() const{
    return this->numFunctions;
}
