
#include <math.h>

#include "LightFromProbe.h"
#include "Bitmap.h"

LightFromProbe::LightFromProbe(const std::string file)
: name(file)
, lightmap(tdogl::Bitmap::bitmapFromFile(file)){
    lightmap.flipVertically();
    
}

glm::vec3 LightFromProbe::light(double theta, double phi) const{
    double w = lightmap.width()/2;
    double h = lightmap.height()/2;
    
    double x = sin(theta)*cos(phi);
    double y = sin(theta)*sin(phi);
    
    unsigned char *color= lightmap.getPixel( w - w*x, h- h*y);
    return glm::vec3(color[0]/256.0,color[1]/256.0,color[2]/256.0);
}

std::string LightFromProbe::getName() const{
    return  this->name;
}