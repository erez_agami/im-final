
#ifndef im_final_IDraw_hpp
#define im_final_IDraw_hpp

#include "Frame.hpp"
#include "IFrameHandler.hpp"
#include "SphericalHarmonics.hpp"
#include <iostream>

typedef struct {
    std::string bgVertexShader;
    std::string bgFragmentShader;
    std::string modelVertexShader;
    std::string modelFragmentShader;
} DrawingResources;
/*Draw the object on incoming frame, using
 information provided by the detector (if available)
 */
class IDraw : public IFrameHandler{
public:
    
    IDraw(const std::string &windowName, DrawingResources dr){
        this->windowName = windowName;
        this->dr = dr;
    }
    
    virtual void changeLight(const ILightSource &lightSource) = 0;
    
    virtual void changeObj(const std::string newObjFile) = 0;
    
    virtual void close() = 0;
    
protected:
    std::string windowName;
    DrawingResources dr;
};

#endif
