#ifndef __im_final__MInstance__
#define __im_final__MInstance__

#include <iostream>
#include "Model.h"
#include "SphericalHarmonics.hpp"

/**
 An Instance of a Model (Different model Matrix)
 */
class MInstance{
public:
    static MInstance newFromModel(Model *m);
    
    void updateModelMatrix(glm::mat4 mat);
    
    void renderInstance(const glm::mat4 &camera, glm::mat4 &view, SHLightCoefficient *light);
    
    
private:
    Model *m;
    glm::mat4 modelMatrix;
    std::vector<glm::vec3*> *light;
    
};
#endif /* defined(__im_final__MInstance__) */
