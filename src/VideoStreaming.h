#ifndef __example2__VideoStreaming__
#define __example2__VideoStreaming__

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "Socket.h"
#include <opencv2/opencv.hpp>
#include "image_convertor.h"

struct video_frame{
    int _id;
    unsigned char *buffer;
    unsigned int modelCode;
    unsigned int lightCode;
    unsigned int length;
};
typedef struct video_frame VideoFrame;
/*Interface that receive incoming frame */
class IStreamListener{
public:
    virtual cv::Mat onFrame(cv::Mat, int modelCode, int lightCode) = 0;
};
/*Open a server over tcp to recieve frame from network source (e.g phone)*/
class VideoStreamer{
public:
    VideoStreamer(int port);
    ~VideoStreamer();
    void start();
    void registerCallback(IStreamListener *);
    void stop();
private:
    Socket *server;
    int port;
    IStreamListener *_cb;
    bool pleaseStop;
    ImageConvertor im;
    
    
};
#endif /* defined(__example2__VideoStreaming__) */
