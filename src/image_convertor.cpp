
#include "image_convertor.h"


cv::Mat ImageConvertor::convert(unsigned char *buff, unsigned long length){
    
    int rc;
    
	// Variables for the decompressor itself
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
    
	// Variables for the output buffer, and how long each row is
	unsigned long bmp_size;
	unsigned char *bmp_buffer;
	int row_stride, width, height, pixel_size;
    
    
    cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);
    
    jpeg_mem_src(&cinfo, buff, length);
    
    rc = jpeg_read_header(&cinfo, TRUE);
    
	if (rc != 1) {
        std::runtime_error("failed to read header");
	}
    
    jpeg_start_decompress(&cinfo);
    
    width = cinfo.output_width;
	height = cinfo.output_height;
	pixel_size = cinfo.output_components;
    
    bmp_size = width * height * pixel_size;
	bmp_buffer = (unsigned char*) malloc(bmp_size);
    
		row_stride = width * pixel_size;
    
  	while (cinfo.output_scanline < cinfo.output_height) {
		unsigned char *buffer_array[1];
		buffer_array[0] = bmp_buffer + \
        (cinfo.output_scanline) * row_stride;
        
		jpeg_read_scanlines(&cinfo, buffer_array, 1);
        
	}
    
  	jpeg_finish_decompress(&cinfo);
    
	jpeg_destroy_decompress(&cinfo);
	    
    
    
    cv::Mat m( height, width, CV_8UC3, bmp_buffer);
    cv::cvtColor(m, m, CV_RGB2BGR);
    free(bmp_buffer);
    return m;
    
}