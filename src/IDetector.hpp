
#ifndef im_final_Detector_hpp
#define im_final_Detector_hpp

#include "Frame.hpp"
#include "IFrameHandler.hpp"
#include "CameraCalibration.hpp"
/*Element that detect a puttern image in incoming frame
 output is stored in the frame object so it will be transfer to the next IFrameHandler
 */
class IDetector : public IFrameHandler{
public:
    virtual void changePatternAndCalibration(const cv::Mat& patternImage, const CameraCalibration& calibration) = 0;;
    
};

#endif
