#ifndef im_final_SphericalHarmonics_h
#define im_final_SphericalHarmonics_h

#include "ILightSource.h"
#include <vector>
#include <glm/glm.hpp>
using namespace std;

struct SHSample{
    float theta;
    float phi;
    glm::vec3 unitVec;
    float *shFuncVal;
};

/* ====================== Math ======================
 */
//Calculate the legendre Polynomial
double Plegendre(int l, int m, double x);
//renormalisation constant for SH function
double Knormalize(int l, int m);
//Calcualte the SH Function at point(spherical coord)
double SHFunc(int l, int m, double theta, double phi);


/*
 ===================== Classes ====================
 */

/*
 * Manage the memory of the random samples
 */
class SHSamples{
public:
    /*Construct new instance with given number of bands, and root number of samples*/
    SHSamples(const unsigned int band, const unsigned  sqrtSample);
    ~SHSamples();
    const SHSample *samplesPtr() const;
    unsigned int getNumFunctions() const;
    unsigned int getNumSamples() const;
    unsigned int getNumBands() const;
private:
    SHSample * samples = NULL;
    unsigned int numBands;
    unsigned int numFunctions;
    unsigned int numSamples;
    
};

/**
 * Manage the memory of the light function coefficent
 */
class SHLightCoefficient  {
public:
    /*Calculate the coefficent using the sample and a light source*/
    SHLightCoefficient(const SHSamples *samples, const ILightSource * lightSource );
    ~SHLightCoefficient();
    void rotate(double theat, double phi);
    const glm::vec3 * getRotatedLightCoefficient() const;
    const unsigned int getNumFunctions() const;
private:
    unsigned int numBands;
    unsigned int numFunctions;
    glm::vec3 * lightCoefficient = NULL;
    glm::vec3 * rotatedLightCoefficient = NULL;
    
};

/**
 * Manage a memory for the coefficent of a verticies functions
 */
class SHVerticesCoefficient  {
public:
    /*Calculate the coefficent using samples,  
     the transfer function is the dot product of sample and vector normal*/
    SHVerticesCoefficient(const SHSamples *samples, const vector<float> normals);
    ~SHVerticesCoefficient();
    const float * getTransferCoefficent() const;
    const unsigned int getNumVertices() const;
    const void applyLight(const SHLightCoefficient *, glm::vec3 result[]) const;
private:
    unsigned int numFunctions;
    unsigned int numVertices;
    float * transferCoefficient = NULL;
};


#endif
