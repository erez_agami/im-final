#ifndef __im_final__LightFromPanorama__
#define __im_final__LightFromPanorama__

#include <iostream>
#include <opencv2/opencv.hpp>
#include "ILightSource.h"
/*
 * Retrieve light value for a given direction from a panoramd image
 */
class LightFromPanorama : public ILightSource{
public:
    LightFromPanorama(const std::string);
    virtual std::string getName() const;
    virtual glm::vec3 light(double theta, double phi) const;
private:
    cv::Mat mat;
    std::string name;
};
#endif /* defined(__im_final__LightFromPanorama__) */
