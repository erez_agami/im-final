
#ifndef __im_final__OpenglDraw__
#define __im_final__OpenglDraw__

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <stdexcept>

#include "GL/glew.h"
#include "GL/glfw.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "IDraw.hpp"
#include "Frame.hpp"
#include "CameraCalibration.hpp"
#include "Program.h"
#include "Model.h"
#include "MInstance.h"
#include "Camera.h"

typedef struct {
    GLuint texID;
    GLuint vao;
    GLuint vbo[2];
    tdogl::Program *shaders;
} GlBackgroundRes;

/*Handle the opengl pipling, draw the model based on light and transformation obtained from The image processing piplein*/
class OpenglDraw : public IDraw{
public:
    /*init opengl load static resource display window*/
    OpenglDraw(const std::string &windowName, DrawingResources dr);
    ~OpenglDraw();
    
    virtual void changeCalibration(const CameraCalibration& calibration){
        this->calibration = calibration;
    }
    virtual void inFrame(PipFrame frame);
    /*Change the light of the secene, will cause re calculation of sh values*/
    virtual void changeLight(const ILightSource &lightSource);
    
    /*Change the displayed object (load from wavefront obj file)*/
    virtual void changeObj(const std::string newObjFile);
    
    /*clean up object opengl and memory*/
    virtual void close();
private:
    //function
    void initGL();
    void cleanGL();
    
    void initModelGL();
    void cleanModelGL();
    
    void initBackgroundGL();
    void cleanBackgroundGL();
    
    void initLight();
    void cleanLight();
    
    glm::mat4 buildProjectionMatrix(const CameraCalibration& calibration, int screen_width, int screen_height);
    
    // in loop
    void drawBackground(cv::Mat frame);
    
   
    //lightStufff
    SHLightCoefficient *light;
    SHSamples * lightSamples;
    double phi = 0.0, theta =0.0;
    
    //model stuff
    std::string objFile;
    Model *model;
    MInstance instance;
    
    //background stuff
    GlBackgroundRes bgr;
    
    CameraCalibration calibration;
    bool isInit = false;
    int width, height;
    
};
#endif /* defined(__im_final__OpenglDraw__) */
