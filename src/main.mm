
#include <Foundation/Foundation.h>
#include <stdio.h>
#include <stdexcept>

#include <opencv2/opencv.hpp>

#include "Server.h"
#include "Texture.h"
#include "Bitmap.h"
#include "LightFromProbe.h"
#include "LightFromPanorama.h"
#include "OpenglDraw.hpp"
#include "ARPDetector.hpp"



const float DIST_COREECTION[5] =
{-0.17212064825897139,6.5362722435783658f
    ,0.0f,0.0f, -4.642326285580673f};

const float CAL_DATA[4] =
{1048.1760993502696f, 1048.1760993502696f
    , 639.50000000000000f, 3.5950000000000000f};

const int PORT = 5000;
const std::string PATTEN_IMAGE = "fiducial.jpg";


// returns the full path to the file `fileName` in the resources directory of the app bundle
std::string resource_path(std::string fileName) {
    NSString* fname = [NSString stringWithCString:fileName.c_str() encoding:NSUTF8StringEncoding];
    NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fname];
    return std::string([path cStringUsingEncoding:NSUTF8StringEncoding]);
}


/*int main(int argc, char * agrv[]){
    CameraCalibration calibration(CAL_DATA[0], CAL_DATA[1], CAL_DATA[2], CAL_DATA[3], DIST_COREECTION);
    
    cv::Mat patternImage = cv::imread(resource_path(PATTEN_IMAGE));
    if (patternImage.empty())
    {
        throw std::runtime_error("Pattern image cannot be read");
    }

    //light info
    //LightFromProbe lightFrom(resource_path("grace_probe.bmp"));
    LightFromPanorama lightFrom(resource_path("ll.jpg"));
    
    //draw Resource
    DrawingResources dr;
    dr.modelVertexShader = resource_path("vertex-shader.vertex");
    dr.modelFragmentShader = resource_path("fragment-shader.fragment");
    dr.bgVertexShader = resource_path("vertex-shader-bg.vertex");
    dr.bgFragmentShader= resource_path("fragment-shader-bg.fragment");
    
    //Building Pipe
    ARPDetector arpDetector;
    OpenglDraw d("Main Window",dr);
    arpDetector.connect(&d);
    
    arpDetector.changePatternAndCalibration(patternImage, calibration);
    
    //setup draw
    d.changeObj(resource_path("Sofa.obj"));
    d.changeLight(lightFrom);
    d.changeCalibration(calibration);
    
    
    
    /*Server server(PORT);
    server.setDr(&dr);
    server.registerHandler(&arpDetector);
    server.startBlocking();
    */
    //
    //cv::Mat m = cv::imread("/Users/erezagami/Downloads/photos/Photo on 5-31-14 at 10.17 PM.jpg");
    
    /*std::string objects[3] = {resource_path("Chair.obj"), resource_path("Leather_Chair.obj"), resource_path("Sofa.obj")};
    ILightSource * lights[4] = {  new LightFromPanorama(resource_path("ld.jpg"))
                                                ,  new LightFromProbe(resource_path("grace_probe.bmp"))
                                                ,  new LightFromProbe(resource_path("uffizi_probe.bmp"))
                                                ,  new LightFromProbe(resource_path("rnl_probe.bmp"))
                                                 };
    int oIndex = 0;
    int lIndex = 0;
    int i = 0;
     
    cv::Mat m(800, 600, CV_8UC3, cv::Scalar(240,240,240));
    while (true){
        
        PipFrame frame;
        frame.frame = m;
        arpDetector.inFrame(frame);
        usleep(500);
    }
    
    
    char c;
    gets(&c);
    
    
    
}*/