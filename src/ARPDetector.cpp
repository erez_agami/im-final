
#include "ARPDetector.hpp"
void ARPDetector::inFrame(PipFrame frame){
    
    frame.patternFound =  patternDetector.findPattern(frame.frame, patternInfo);
    
    if (frame.patternFound)
    {
        patternInfo.computePose(pattern,calibration);
    }
    
    frame.transformation = patternInfo.pose3d;
    
    if (NULL != this->next){
        this->next->inFrame(frame);
    }
}

void ARPDetector::changePatternAndCalibration(const cv::Mat& patternImage, const CameraCalibration& calibration){
    patternDetector.buildPatternFromImage(patternImage, pattern);
    patternDetector.train(pattern);
    this->calibration = calibration;
}