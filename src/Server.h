
#ifndef __im_final__Server__
#define __im_final__Server__

#include <iostream>
#include <vector>
#include "VideoStreaming.h"
#include "IFrameHandler.hpp"
#include "Texture.h"
#include "ILightSource.h"
#include "IDraw.hpp"

/* A Server for streaming the frames in and out */
class Server : public IStreamListener{
public:
    Server( const int port);
    ~Server();
    
    void registerHandler(IFrameHandler *frameHandler);
    
    void setDr(DrawingResources *dr);
    void startBlocking();
    void stopServer();
    
    virtual cv::Mat onFrame(cv::Mat, int modelCode, int lightCode);
    
private:
    int port;
    IFrameHandler *frameHandler = NULL;
    VideoStreamer *streamer = NULL;
    IDraw *drawHandler = NULL;
    std::vector<std::string> objectOptions;
    std::vector<ILightSource*> lightSourceOptions;
    int lightOption =1;
    int modelOption =1;
    DrawingResources *dr;
    
    
};
#endif
