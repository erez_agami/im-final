#include <stdio.h>
#include "SphericalHarmonics.hpp"
#include <stdexcept>
#include "math.h"


/**/
SHSamples::SHSamples(const unsigned int bands, const unsigned  sqrtSample)
: numSamples(sqrtSample*sqrtSample)
,numBands(bands)
,numFunctions(bands * bands)
{
    
    this->samples = new SHSample[this->numSamples];
    for (int i=0; i<this->numSamples;i++){
        this->samples[i].shFuncVal = new float[numFunctions];
        
        double x = (rand()/(double)RAND_MAX);
        double y = (rand()/(double)RAND_MAX);
        
        //convert to spherical
        double theta = 2.0 * acos(sqrt(1.0 - x));
        double phi = this->samples[i].phi = 2.0 * M_PI * y;
        
        this->samples[i].theta = theta;
        this->samples[i].phi = phi;
        
        this->samples[i].unitVec = glm::vec3(sin(theta)*cos(phi),
                                               sin(theta)*sin(phi),
                                               cos(theta));
        //calculate coeff for sample
        for (int l=0; l<numBands;++l){
            for (int m=-l; m<=l;++m){
                int index = l*(l+1)+m;
                samples[i].shFuncVal[index] = SHFunc(l,m,theta,phi);
            }
        }
    }
    
    
    
    
}
SHSamples::~SHSamples(){
    if (NULL != this->samples){
        for (int i=0; i<numSamples; i++){
            delete samples[i].shFuncVal;
        }
        delete this->samples;
    }
}
const SHSample * SHSamples::samplesPtr() const{
    return this->samples;
}
unsigned int SHSamples::getNumFunctions() const{
    return this->numFunctions;
}
unsigned int SHSamples::getNumSamples() const{
    return this->numSamples;
}
unsigned int SHSamples::getNumBands() const{
    return this->numBands;
}


