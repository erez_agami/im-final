#include <math.h>
#include "LightFromPanorama.h"

LightFromPanorama::LightFromPanorama(const std::string fname) : name(fname){
    this->mat = cv::imread(fname);
}
glm::vec3 LightFromPanorama::light(double theta, double phi) const{
    int w = this->mat.rows;
    int h = this->mat.cols;
    
    int x = w * theta/(2 *M_PI);
    int y = h * phi/(M_PI);
    cv::Vec3b v= this->mat.at<cv::Vec3b>(x,y);
    
    return glm::vec3(v[0]/256.0,v[1]/256.0,v[2]/256.0);
    
}


std::string LightFromPanorama::getName() const{
    return  this->name;
}
