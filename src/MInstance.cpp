#include "MInstance.h"

MInstance MInstance::newFromModel(Model *m){
    MInstance mi;
    mi.m = m;
    mi.light = NULL;
    return mi;
}

void MInstance::updateModelMatrix(glm::mat4 mat){
    this->modelMatrix = mat;
}

void MInstance::renderInstance(const glm::mat4 &camera, glm::mat4 &view, SHLightCoefficient *light){
    this->m->render(camera, view, this->modelMatrix, light);
}