#include <stdio.h>
#include "SphericalHarmonics.hpp"

SHVerticesCoefficient::SHVerticesCoefficient(const SHSamples *samples, const vector<float> normals)
: numFunctions(samples->getNumFunctions())
{
    numVertices = (unsigned int)(normals.size()/3);
    const unsigned int numSample = samples->getNumSamples();
    const unsigned int numFunctions = samples->getNumFunctions();
    const SHSample * samplesArray = samples->samplesPtr();
    
    this->transferCoefficient = new float[numFunctions * numVertices];
    float *currVertexCoeff = this->transferCoefficient;
    
    for (int j = 0; j< normals.size(); j+=3){
        
        float x,y,z;
        x = normals[j];
        y = normals[j+1];
        z = normals[j+2];
        glm::vec3  norm= glm::vec3(x,y,z);
        
        for (int k=0; k< numFunctions; ++k){
            currVertexCoeff[k] = 0.0f;
        }
        
        for (int k=0; k< numSample; ++k){
            float dot = (float)glm::dot(samplesArray[k].unitVec,norm);
            if (dot > 0.0){
                for (int l=0; l< numFunctions; ++l){
                    currVertexCoeff[l] += dot * samplesArray[k].shFuncVal[l];
                }
            }
        }
        
        for (int k=0;k <numFunctions;++k){
            currVertexCoeff[k]*=4*M_PI/numSample;
        }
        currVertexCoeff += numFunctions;
    }
}
SHVerticesCoefficient::~SHVerticesCoefficient(){
    if (NULL != this->transferCoefficient){
        delete transferCoefficient;
    }
}
const float * SHVerticesCoefficient::getTransferCoefficent() const{
    return this->transferCoefficient;
}

const unsigned int SHVerticesCoefficient::getNumVertices() const{
    return this->numVertices;
}

const void SHVerticesCoefficient::applyLight(const SHLightCoefficient *light, glm::vec3 result[]) const{
    float * currVertexCoeff = this->transferCoefficient;
    const glm::vec3 * rotatedCoeff = light->getRotatedLightCoefficient();
    for (int i=0; i< numVertices; i++){
        result[i] = glm::vec3(0.0,0.0,0.0);
        for (int j=0; j<numFunctions;j++){
            result[i]+=rotatedCoeff[j] * currVertexCoeff[j];
        }
        currVertexCoeff+=this->numFunctions;
    }
}