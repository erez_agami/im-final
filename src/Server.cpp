#include "Server.h"

Server::Server(const int port){
    this->streamer = new VideoStreamer(port);

}


Server::~Server(){
    stopServer();
}
void Server::startBlocking(){
    this->streamer->registerCallback(this);
    this->streamer->start();
    
}
cv::Mat Server::onFrame(cv::Mat aFrame, int modelCode, int lightCode){
    PipFrame frame;
    frame.frame = aFrame;
    if (this->modelOption != modelCode || this->lightOption != lightCode){
        this->modelOption = modelCode;
        this->lightOption = lightCode;
        //dr->objFile = objectOptions[modelCode];
        //dr->shHandler->setLightSource(this->lightSourceOptions[lightCode]);
        
        //drawHandler->changeDrawingResource(*this->dr);
    }
    if (NULL != this->frameHandler){
        this->frameHandler->inFrame(frame);
        return frame.outputFrame;
    }
    return cv::Mat();
}

void Server::setDr(DrawingResources *dr){
    this->dr = dr;
}

void Server::registerHandler(IFrameHandler *frameHandler){
    this->frameHandler = frameHandler;
}

void Server::stopServer(){
    if (NULL != this->streamer ){
        this->streamer->stop();
    }
    this->streamer = NULL;
    
}