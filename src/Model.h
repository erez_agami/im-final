#ifndef __im_final__Model__
#define __im_final__Model__

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <stdexcept>

#include "GL/glew.h"
#include "GL/glfw.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "IDraw.hpp"
#include "Frame.hpp"
#include "CameraCalibration.hpp"
#include "Program.h"
#include "Texture.h"
#include "tiny_obj_loader.h"
#include "SphericalHarmonics.hpp"
//Support sturct for storing shapes that make up a model
typedef struct {
    GLuint vbo[5]; //buffer handles
    GLenum drawType;
    GLint drawCount;
    tdogl::Texture *labedo; // the model diffision value per pixel
    tinyobj::shape_t  rawshape; // the raw shape (from obj)
    SHVerticesCoefficient *verticesLightCoeff; //shperical haramonic coefficents for vertices light transfer functions
    glm::vec3* tempColorValueBuffer; //temp buffer used for storing actual light vaule for verticies before draw
} ShapeInModel;
/**
 Represent an opengl drawable model
 it contain method to load and draw a 3d model using a light 
 */
class Model{
public:
    //Create new model form OBJ file
    Model(const std::string objFile
                , const std::string vertexShader
                , const std::string fragmentshader
                , const SHSamples *samples);
    
    
    /*Render the model using the binded final light buffer*/
    void render(const glm::mat4 &camera,const glm::mat4 &view,const glm::mat4 &modelTransform, const SHLightCoefficient *light) ;
    
    
    //Load the model into Opengl buffer
    void load();
    //Unload the model from opengl buffers
    void unload();
    
    ~Model();
protected:
    void initLight(const SHSamples * samples);
    void cleanLight();
    
    void bindLightBuffer(const SHLightCoefficient *light);
    
    void normalize();
    
    std::vector<ShapeInModel> shapes;
    tdogl::Program *shaders;
    GLuint vao;
    std::string file;
};

#endif /* defined(__im_final__Model__) */
