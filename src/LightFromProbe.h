#ifndef __im_final__LightFromProbe__
#define __im_final__LightFromProbe__

#include <iostream>
#include "Bitmap.h"
#include "ILightSource.h"

/* return light source value for a given direction probe image*/
class LightFromProbe : public ILightSource{
public:
    LightFromProbe(const std::string);
    virtual std::string getName() const;
    virtual glm::vec3 light(double theta, double phi) const;
private:
        tdogl::Bitmap lightmap;
        std::string name;
};
#endif /* defined(__im_final__LightFromProbe__) */
