
#ifndef __im_final__ARPDetector__
#define __im_final__ARPDetector__


#include <iostream>
#include "Frame.hpp"
#include "IFrameHandler.hpp"
#include "IDetector.hpp"
#include "PatternDetector.hpp"
#include "CameraCalibration.hpp"
#include "GeometryTypes.hpp"
/*Use Opencv to detect the location of a pattern image on incoming frames*/
class ARPDetector : public IDetector{
public:
    virtual void inFrame(PipFrame frame);
    virtual void changePatternAndCalibration(const cv::Mat& patternImage, const CameraCalibration& calibration);
private:
    CameraCalibration   calibration;
    FPattern            pattern;
    PatternTrackingInfo patternInfo;
    PatternDetector     patternDetector;
};
#endif /* defined(__im_final__ARPDetector__) */
