

#include "OpenglDraw.hpp"
#include "math.h"
#define BG_SHADER
#define RAD_TO_DGREE 57.2957795
const int VETEX_BUFF =0;
const int UV_BUFF =1;

/*Init Global OPENGL Stuff */
void OpenglDraw::initGL(){
    if(!glfwInit())
        throw std::runtime_error("glfwInit failed");
    width = 800;
    height = 600;
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
    glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
    if(!glfwOpenWindow((int)width, (int)height, 8, 8, 8, 8, 16, 0, GLFW_WINDOW))
        throw std::runtime_error("glfwOpenWindow failed. Can your hardware handle OpenGL 3.2?");
    
    glfwSetWindowTitle(this->windowName.c_str());
    
    // GLFW settings
    //glfwDisable(GLFW_MOUSE_CURSOR);
    glfwSetMousePos(0, 0);
    glfwSetMouseWheel(0);
    
    // initialise GLEW
    glewExperimental = GL_TRUE; //stops glew crashing on OSX :-/
    if(glewInit() != GLEW_OK)
        throw std::runtime_error("glewInit failed");
    
    // GLEW throws some errors, so discard all the errors so far
    while(glGetError() != GL_NO_ERROR) {}
    
    // print out some info about the graphics drivers
    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
    
    // make sure OpenGL version 3.2 API is available
    if(!GLEW_VERSION_3_2)
        throw std::runtime_error("OpenGL 3.2 API is not available.");
    
    // OpenGL settings
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}
/*Close OpenGL and window*/
void OpenglDraw::cleanGL(){
    glfwTerminate();
}

/*Init Background Shader and resources*/
void OpenglDraw::initBackgroundGL(){
    if(NULL == this->bgr.shaders){
        glGenTextures(1, &this->bgr.texID);
        glGenVertexArrays(1, &this->bgr.vao);
        glGenBuffers(2, this->bgr.vbo);
    }
    
    std::vector<tdogl::Shader> shaders;
    shaders.push_back(tdogl::Shader::shaderFromFile(dr.bgVertexShader, GL_VERTEX_SHADER));
    shaders.push_back(tdogl::Shader::shaderFromFile(dr.bgFragmentShader, GL_FRAGMENT_SHADER));
    this->bgr.shaders =  new tdogl::Program(shaders);

}

/*clean Background GL Resource*/
void OpenglDraw::cleanBackgroundGL(){
    if (NULL != this->bgr.shaders){
        delete this->bgr.shaders;
        glDeleteVertexArrays(1, &this->bgr.vao);
        glDeleteBuffers(2, this->bgr.vbo);
        this->bgr.shaders = NULL;
    }
    
}

/* init model GL related Resource */
void OpenglDraw::initModelGL(){
    if (NULL != this->model ){
        delete this->model;
    }
    this->model = new Model(objFile, dr.modelVertexShader, dr.modelFragmentShader, this->lightSamples);
    this->model->load();
    
    this->instance = MInstance::newFromModel(this->model);
    this->instance.updateModelMatrix(glm::mat4());
    
}

/*clean model GL Resource */
void OpenglDraw::cleanModelGL(){
    delete this->model;
    this->model = NULL;
}

/*Init light resource*/
void OpenglDraw::initLight(){
    if (NULL != this->lightSamples){
        throw std::runtime_error("init light should be called once");
    }
    this->lightSamples = new SHSamples(4,100);
}
/*Clean light resource*/
void OpenglDraw::cleanLight(){
    if (NULL != this->lightSamples){
        delete this->lightSamples;
        this->lightSamples = NULL;
    }
    if (NULL != this->light){
        delete this->light;
        this->light = NULL;
    }
}


OpenglDraw::OpenglDraw(const std::string &windowName, DrawingResources dr): IDraw(windowName, dr) {
    this->bgr.texID = 0;
    this->bgr.vao = 0;
    this->bgr.shaders = NULL;
    this->model = NULL;
    this->next = NULL;
    this->lightSamples = NULL;
    this->light = NULL;
    
    initLight();
    initGL();
    initBackgroundGL();
    //initModelGL();
    this->isInit = true;
    
}

OpenglDraw::~OpenglDraw(){
    close();
}
/*Draw a background image (usually the camera image)*/
void OpenglDraw::drawBackground(cv::Mat frame){
    glDisable(GL_DEPTH_TEST);
    
    GLfloat w = frame.cols;
    GLfloat h = frame.rows;
    
    //bind texture
    glBindTexture(GL_TEXTURE_2D, this->bgr.texID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 w,
                 h,
                 0,
                 GL_BGR_EXT,
                 GL_UNSIGNED_BYTE,
                 frame.ptr());
    glBindTexture(GL_TEXTURE_2D, 0);
    
    
    this->bgr.shaders->use();
    glBindVertexArray(this->bgr.vao);
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, this->bgr.texID);
    this->bgr.shaders->setUniform("videoFrame", 2);
    
    const GLfloat bgTextureVertices[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        1.0f,  1.0f
    };
    const GLfloat bgTextureCoords[]   = {
        0.0f,  1.0f,
        1.0f, 1.0f,
        0.0f,  0.0f,
        1.0f, 0.0f
        
    };
    
    glBindBuffer(GL_ARRAY_BUFFER, this->bgr.vbo[UV_BUFF]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bgTextureCoords), bgTextureCoords, GL_STATIC_DRAW );
    glVertexAttribPointer(this->bgr.shaders->attrib("inputTextureCoordinate"),2, GL_FLOAT, GL_FALSE, 0 , NULL);
    glEnableVertexAttribArray(this->bgr.shaders->attrib("inputTextureCoordinate"));
    
    glBindBuffer(GL_ARRAY_BUFFER, this->bgr.vbo[VETEX_BUFF]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bgTextureVertices), bgTextureVertices, GL_STATIC_DRAW );
    glVertexAttribPointer(this->bgr.shaders->attrib("position"),2, GL_FLOAT, GL_FALSE, 0 , NULL);
    glEnableVertexAttribArray(this->bgr.shaders->attrib("position"));
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(this->bgr.shaders->attrib("position"));
    glDisableVertexAttribArray(this->bgr.shaders->attrib("inputTextureCoordinate"));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    this->bgr.shaders->stopUsing();
    glBindVertexArray(0);
    glEnable(GL_DEPTH_TEST);
}
void readBufferFromGL(PipFrame frame){
    //unsigned char *image = (unsigned char *)malloc(width * height * sizeof(GLubyte) * 3);
    frame.outputFrame = cv::Mat(frame.frame.rows, frame.frame.cols, CV_8UC3);
    //frame.outputFrame = cv::Mat(height,width,CV_8UC3);
    glPixelStorei(GL_PACK_ALIGNMENT, (frame.outputFrame.step & 3) ? 1 : 4);
    glPixelStorei(GL_PACK_ROW_LENGTH, (unsigned int)frame.outputFrame.step/frame.outputFrame.elemSize());
    glReadPixels(0, 0,  frame.outputFrame.cols ,  frame.outputFrame.rows, GL_BGR, GL_UNSIGNED_BYTE, frame.outputFrame.ptr());
    cv::flip(frame.outputFrame, frame.outputFrame,0);
}

/* Handle new incoming frame*/
void OpenglDraw::inFrame(PipFrame frame){
    if (width != frame.frame.cols || height != frame.frame.rows){
        width = frame.frame.cols;
        height = frame.frame.rows;
        glfwSetWindowSize(width, height);
        glViewport(0, 0, width, height);
    }
    glClearColor(1, 1, 1, 1); // white
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glm::mat4 cmatrix;
    glm::mat4 vmatrix;
    if (!frame.patternFound){
        tdogl::Camera camera;
        camera.setPosition(glm::vec3(3 ,3, 3));
        camera.setViewportAspectRatio(width / height);
        camera.lookAt(glm::vec3(0,0,0));
        cmatrix = camera.matrix();
    }else{
        vmatrix = glm::make_mat4x4(frame.transformation.getMat44().data);
        vmatrix = glm::rotate(vmatrix, 90.0f, glm::vec3(1.0,0.0,0.0));
        cmatrix = buildProjectionMatrix(calibration, frame.frame.cols, frame.frame.rows);

    }
    
    
    //for testing
    //theta +=1;
    //phi += 1;
    //if (theta >= 360) theta-=360;
    //if (phi >= 180) phi-= 180;
    //std::cout << "theat" << theta << "phi"<< phi << "\n";
    //std::cout << "theat" << theta << "phi: "<<phi;
    
    //extract rotation from frame
    //theta = -asin(vmatrix[2][0]) * RAD_TO_DGREE;
    //phi = atan2(vmatrix[2][1],vmatrix[2][2]) * RAD_TO_DGREE;
    std::cout << "t: " << theta << "p: " << phi << "\n";
    //theta = 180.0;
    
    this->light->rotate(theta, phi);
    
    //do stuff
    this->drawBackground(frame.frame);
    glm::mat4 view;
    //print error
    
    this->instance.updateModelMatrix(vmatrix);
    this->instance.renderInstance(cmatrix, view ,light);
    
    
    //print error
    GLenum error = glGetError();
    if(error != GL_NO_ERROR)
        std::cerr << "OpenGL Error " << error << ": " << (const char*)gluErrorString(error) << std::endl;

    readBufferFromGL(frame);
    
    glfwSwapBuffers();
    
    if (NULL != this->next ){
        this->next->inFrame(frame);
    }
}



void OpenglDraw::changeLight(const ILightSource &alight){
    if (NULL != this->light){
        delete this->light;
    }
    this->light = new SHLightCoefficient(lightSamples, &alight);
}

void OpenglDraw::changeObj(const std::string newObjFile){
    this->cleanModelGL();
    this->objFile = newObjFile;
    this->initModelGL();
}

void OpenglDraw::close(){
    cleanLight();
    cleanBackgroundGL();
    cleanModelGL();
    cleanGL();
}

glm::mat4 OpenglDraw::buildProjectionMatrix(const CameraCalibration& calibration, int screen_width, int screen_height)
{
    glm::mat4 mat;
    float nearPlane = 0.01f;  // Near clipping distance
    float farPlane  = 100.0f;  // Far clipping distance
    Matrix44 projectionMatrix;
    // Camera parameters
    float f_x = calibration.fx(); // Focal length in x axis
    float f_y = calibration.fy(); // Focal length in y axis (usually the same?)
    float c_x = calibration.cx(); // Camera primary point x
    float c_y = calibration.cy(); // Camera primary point y
    
    projectionMatrix.data[0] = -2.0f * f_x / screen_width;
    projectionMatrix.data[1] = 0.0f;
    projectionMatrix.data[2] = 0.0f;
    projectionMatrix.data[3] = 0.0f;
    
    projectionMatrix.data[4] = 0.0f;
    projectionMatrix.data[5] = 2.0f * f_y / screen_height;
    projectionMatrix.data[6] = 0.0f;
    projectionMatrix.data[7] = 0.0f;
    
    projectionMatrix.data[8] = 2.0f * c_x / screen_width - 1.0f;
    projectionMatrix.data[9] = 2.0f * c_y / screen_height - 1.0f;
    projectionMatrix.data[10] = -( farPlane + nearPlane) / ( farPlane - nearPlane );
    projectionMatrix.data[11] = -1.0f;
    
    projectionMatrix.data[12] = 0.0f;
    projectionMatrix.data[13] = 0.0f;
    projectionMatrix.data[14] = -2.0f * farPlane * nearPlane / ( farPlane - nearPlane );
    projectionMatrix.data[15] = 0.0f;
    
    mat = glm::make_mat4x4(projectionMatrix.data);
    return mat;
    
}
