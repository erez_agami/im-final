#include "Model.h"

const int VETEX_BUFF =0;
const int UV_BUFF =1;
//const int NORMAL_BUFF =2;
const int INDEX_BUFF =3;
const int LIGHT_BUFF =4;

/*Load Shader from file*/
static tdogl::Program *load_shaders(std::string vertexShader, std::string fragement){
    std::vector<tdogl::Shader> shaders;
    shaders.push_back(tdogl::Shader::shaderFromFile(vertexShader, GL_VERTEX_SHADER));
    shaders.push_back(tdogl::Shader::shaderFromFile(fragement, GL_FRAGMENT_SHADER));
    return new tdogl::Program(shaders);
}
/*Normalize the model to a unit cube size*/
void Model::normalize(){
    double max = 0;;
    for (int i=0; i<shapes.size(); i++){
        tinyobj::shape_t &shape = shapes[i].rawshape;
        
        for (int j=0; j<shape.mesh.positions.size();j++){
            float curr = abs(shape.mesh.positions[j]);
            if (curr > max){
                max = curr;
            }
        }
    }
    if (max <= 1.0){
        return;
    }
    for (int i=0; i<shapes.size(); i++){
        tinyobj::shape_t &shape = shapes[i].rawshape;
        
        for (int j=0; j<shape.mesh.positions.size();j++){
            shape.mesh.positions[j]/=max;
        }
    }
}
/*create model*/
Model::Model(const std::string objFile
                           , const std::string vertexShader
                                  , const std::string fragmentshader
             ,const SHSamples *samples){
    std::vector<tinyobj::shape_t> shapes;
    
    std::string res = tinyobj::LoadObj(shapes, objFile.c_str());
    if (!res.empty()){
        throw std::runtime_error("failed to load obj" + res);
    }
   
    for (int i=0; i<shapes.size(); i++){
        ShapeInModel shape;
        shape.rawshape = shapes[i];
        shape.verticesLightCoeff = NULL;
        shape.labedo = NULL;
        shape.tempColorValueBuffer = NULL;
        
        this->shapes.push_back(shape);
    }
    normalize();
    
    tdogl::Program *p =load_shaders(vertexShader, fragmentshader);
    this->shaders = p;
    
    this->initLight(samples);
}
/*init light coeff for model vetrices*/
void Model::initLight(const SHSamples *samples){
    for (int i=0; i< this->shapes.size(); i++){
        ShapeInModel &shape = shapes[i];
        shape.verticesLightCoeff = new SHVerticesCoefficient(samples, shape.rawshape.mesh.normals);
        shape.tempColorValueBuffer = new glm::vec3[shape.verticesLightCoeff->getNumVertices() * samples->getNumFunctions()];
    }
}
/*clean light memory*/
void Model::cleanLight(){
    for (int i=0; i< this->shapes.size(); i++){
        ShapeInModel &shape = shapes[i];
        if (NULL != shape.verticesLightCoeff){
            delete shape.verticesLightCoeff;
            shape.verticesLightCoeff = NULL;
        }
        if (NULL != shape.tempColorValueBuffer){
            delete shape.tempColorValueBuffer;
            shape.tempColorValueBuffer = NULL;
        }
    }
}
/*allocate opengl buffers and load model resource into them */
void Model::load(){
    glGenVertexArrays(1, &this->vao);
    glBindVertexArray(this->vao);
    
    std::vector<ShapeInModel>::iterator it=  this->shapes.begin();
    for(; it != this->shapes.end(); ++it){
        ShapeInModel &shape = *it;
        
        //Verticies
        glGenBuffers(1, shape.vbo);
        glBindBuffer(GL_ARRAY_BUFFER ,shape.vbo[VETEX_BUFF]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) *  shape.rawshape.mesh.positions.size(),  shape.rawshape.mesh.positions.data(), GL_STATIC_DRAW);
        
        //Texture (Optional)
        if (shape.rawshape.mesh.texcoords.size() > 0){
            glGenBuffers(1, shape.vbo+1);
            glBindBuffer(GL_ARRAY_BUFFER, shape.vbo[UV_BUFF]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(float) *  shape.rawshape.mesh.texcoords.size(),  shape.rawshape.mesh.texcoords.data(), GL_STATIC_DRAW);
        }else{
            shape.vbo[UV_BUFF] = 0;
        }
        
        //for light
        glGenBuffers(1, shape.vbo+4);

        //Indcies
        glGenBuffers(1, shape.vbo+3);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.vbo[INDEX_BUFF]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * shape.rawshape.mesh.indices.size(), shape.rawshape.mesh.indices.data(), GL_STATIC_DRAW);
        
        shape.drawCount = shape.rawshape.mesh.indices.size();
        shape.drawType = GL_TRIANGLES;
        
        if (!shape.rawshape.material.diffuse_texname.empty()){
            tdogl::Bitmap dt_bmp = tdogl::Bitmap::bitmapFromFile(shape.rawshape.material.diffuse_texname);
            dt_bmp.flipVertically();
            shape.labedo = new tdogl::Texture(dt_bmp,GL_LINEAR,GL_REPEAT);
        }

        
    }
    
    // unbind the VBO and VAO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

/*Generate Light value for vertices and bind them into opengl buffer*/
void Model::bindLightBuffer(const SHLightCoefficient *light){
    
    glBindVertexArray(this->vao);
    std::vector<ShapeInModel>::iterator it=  this->shapes.begin();

    for(; it != this->shapes.end(); ++it){
        ShapeInModel &shape = *it;
        const unsigned int valueCount = light->getNumFunctions() * shape.verticesLightCoeff->getNumVertices();
        
        shape.verticesLightCoeff->applyLight(light, shape.tempColorValueBuffer);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.vbo[LIGHT_BUFF]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glm::vec3) *  valueCount, shape.tempColorValueBuffer, GL_STATIC_DRAW);
        

    }
    glBindVertexArray(0);
}

void Model::render(const glm::mat4 &camera,const glm::mat4 &view, const glm::mat4 &modelTransform, const SHLightCoefficient *light) {
    this->shaders->use();
    this->bindLightBuffer(light);
    
    glBindVertexArray(this->vao);

    shaders->setUniform("camera", camera);
    shaders->setUniform("light_only", false);
    shaders->setUniform("model", modelTransform);
    
    
 
    for (int i =0; i < this->shapes.size(); i++){
        ShapeInModel &shape = this->shapes[i];
        tinyobj::shape_t &shpe_type = shape.rawshape;
        
        tinyobj::material_t mat =  shpe_type.material;
        
        if (NULL != shape.labedo ){
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, shape.labedo->object());
            shaders->setUniform("tex", 0);
            shaders->setUniform("use_gloabl", false);
        }else{
            shaders->setUniform("use_gloabl", true);
            float *kd = shape.rawshape.material.diffuse;
            shaders->setUniform("global_diff", glm::vec3(kd[0],kd[1],kd[2]) * 0.7f);
        }
        
        glEnableVertexAttribArray(this->shaders->attrib("vert"));
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo[VETEX_BUFF]);
        glVertexAttribPointer(this->shaders->attrib("vert"), 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        if (shape.vbo[UV_BUFF] > 0){ // only if buffer was created
            glEnableVertexAttribArray(this->shaders->attrib("vert_texspace"));
            glBindBuffer(GL_ARRAY_BUFFER, shape.vbo[UV_BUFF]);
            glVertexAttribPointer(this->shaders->attrib("vert_texspace"), 2, GL_FLOAT, GL_FALSE,  0, NULL);
        }
        
        
        glEnableVertexAttribArray(this->shaders->attrib("light_val"));
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo[LIGHT_BUFF]);
        glVertexAttribPointer(this->shaders->attrib("light_val"), 3, GL_FLOAT, GL_FALSE,  0, NULL);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.vbo[INDEX_BUFF]);
        
        // Draw the triangle !
        glDrawElements(shape.drawType, shape.drawCount, GL_UNSIGNED_INT, (void *) 0);
        
        
        //Cleanup
        if (NULL != shape.labedo){
            glBindTexture(GL_TEXTURE_2D, 0);
        }
        
        if (shape.vbo[UV_BUFF]){
            glDisableVertexAttribArray(this->shaders->attrib("vert_texspace"));
        }
        
        glDisableVertexAttribArray(this->shaders->attrib("vert"));
        glDisableVertexAttribArray(this->shaders->attrib("light_val"));
        
    }
    
    glBindVertexArray(0);
    shaders->stopUsing();
}

void Model::unload(){
    glDeleteVertexArrays(1, &this->vao);
    
    std::vector<ShapeInModel>::iterator it=  this->shapes.begin();
    for(; it != this->shapes.end(); ++it){
        ShapeInModel &shape = *it;
        glDeleteBuffers(1, shape.vbo);
        glDeleteBuffers(1, shape.vbo+1);
        glDeleteBuffers(1, shape.vbo+2);
        glDeleteBuffers(1, shape.vbo+3);

    }
}

Model::~Model(){
    cleanLight();
    if (NULL != this->shaders ){
        delete this->shaders;
        this->shaders = NULL;
    }
    for (int i=0; i< this->shapes.size(); i++){
        ShapeInModel * shape = &shapes[i];
        if (NULL != shape->labedo){
            delete shape->labedo;
            shape->labedo = NULL;
        }
    }
    unload();
}