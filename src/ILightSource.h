
#ifndef im_final_ILightSource_h
#define im_final_ILightSource_h

#include <iostream>
#include <glm/glm.hpp>

/* abstract interface for a spherical light source */
class ILightSource{
public:
    virtual std::string getName() const = 0;
    virtual glm::vec3 light(double theta, double phi) const = 0;
};


#endif
