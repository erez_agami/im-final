
#ifndef im_final_Frame_hpp
#define im_final_Frame_hpp
#include "GeometryTypes.hpp"
#include "CameraCalibration.hpp"
#include <opencv2/opencv.hpp>
/*Frame Processed in pipline*/
class PipFrame{
public:
    cv::Mat frame;
    bool patternFound = false;
    Transformation transformation;
    cv::Mat outputFrame;
private:
    
};

#endif
